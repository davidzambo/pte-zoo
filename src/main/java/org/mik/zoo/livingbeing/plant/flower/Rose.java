package org.mik.zoo.livingbeing.plant.flower;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.mik.zoo.livingbeing.Color;
import org.mik.zoo.livingbeing.plant.PlantType;
import org.mik.zoo.livingbeing.plant.tree.Oak;

public class Rose extends AbstractFlower {
	
    public static final String SCIENTIFIC_NAME = "Rosa"; //$NON-NLS-1$
    public static final String IMAGE_URI = "https://en.wikipedia.org/wiki/Rose#/media/File:Rosa_rubiginosa_1.jpg"; //$NON-NLS-1$
    private static final int NUMBER_OF_BUDS = 1;
    private static final int MAX_AGE = 4;
    private static final int MAX_HEIGHT = 2;

	public Rose() {
	}

	public Rose(Integer id, String scientificName, String instanceName, String imageURI, int maxAge, int maxHeight,
			PlantType plantType, int numberOfBuds, Color colorOfBuds) {
		super(id, scientificName, instanceName, imageURI, maxAge, maxHeight, plantType, numberOfBuds, colorOfBuds);
	}

	public Rose(String instanceName) {
		super(null, SCIENTIFIC_NAME, instanceName, IMAGE_URI, MAX_AGE, MAX_HEIGHT, PlantType.FLOWER, NUMBER_OF_BUDS, Color.MULTIPLE);
	}

	public Rose(String instanceName, Color color) {
		super(null, SCIENTIFIC_NAME, instanceName, IMAGE_URI, MAX_AGE, MAX_HEIGHT, PlantType.FLOWER, NUMBER_OF_BUDS, color);
	}


	public Rose(ResultSet rs) throws SQLException {
		super(rs);
		this.setColorOfBuds(Color.fromInt(rs.getInt(COL_COLOR_OF_BUDS)));
	}
	
	public static Rose createFromResultSet(ResultSet rs) throws SQLException {
		return new Rose(rs);
	}
	
	@Override
	public String toString() {
		return "Rose:" + super.toString(); //$NON-NLS-1$
	}


}

package org.mik.zoo.livingbeing.plant;

import org.mik.zoo.livingbeing.Livingbeing;

public interface Plant extends Livingbeing{
	
	public final static String COL_PLANT_TYPE = "plant_type"; //$NON-NLS-1$
	public final static String COL_MAX_AGE = "max_age"; //$NON-NLS-1$
	public final static String COL_MAX_HEIGHT= "max_height"; //$NON-NLS-1$
	
	int getMaxAge();
	
	int getMaxHeight();
		
	PlantType getPlantType();
}

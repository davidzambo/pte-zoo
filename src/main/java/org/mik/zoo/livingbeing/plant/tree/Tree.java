package org.mik.zoo.livingbeing.plant.tree;

public interface Tree {
	
	public final static String COL_BARK_THICKNESS = "bark_thickness"; //$NON-NLS-1$

	float getBarkThickness();
}

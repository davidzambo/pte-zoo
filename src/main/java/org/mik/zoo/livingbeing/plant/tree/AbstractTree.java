package org.mik.zoo.livingbeing.plant.tree;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mik.zoo.livingbeing.plant.AbstractPlant;
import org.mik.zoo.livingbeing.plant.PlantType;
import org.mik.zoo.livingbeing.plant.flower.Flower;

public abstract class AbstractTree extends AbstractPlant implements Tree {
	private static List<Tree> allTrees = new ArrayList<>();
	
	private float barkThickness;

	public AbstractTree() {
	}

	/**
	 * @return the allTrees
	 */
	public static List<Tree> getAllTrees() {
		return allTrees;
	}

	/**
	 * @param allTrees the allTrees to set
	 */
	public static void setAllTrees(List<Tree> allTrees) {
		AbstractTree.allTrees = allTrees;
	}

	/**
	 * @return the barkThickness
	 */
	@Override
	public float getBarkThickness() {
		return this.barkThickness;
	}

	/**
	 * @param barkThickness the barkThickness to set
	 */
	public void setBarkThickness(float barkThickness) {
		this.barkThickness = barkThickness;
	}

	public AbstractTree(Integer id, String scientificName, String instanceName, String imageURI, int maxAge,
			int maxHeight, PlantType plantType, float barkThickness) {
		super(id, scientificName, instanceName, imageURI, maxAge, maxHeight, plantType);
		this.barkThickness = barkThickness;
		registerTree(this);
	}

	public AbstractTree(ResultSet rs) throws SQLException {
		super(rs);
		this.barkThickness = rs.getFloat(COL_BARK_THICKNESS);
		//registerTree(this);
	}
	
	@Override
	public boolean isTree() {
		return true;
	}

	
    @Override
    public String getUpdateSql() {
            return String.format("%s, %s=%f", super.getUpdateSql(), //$NON-NLS-1$
            		COL_BARK_THICKNESS, Float.valueOf(getBarkThickness()));
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
            super.getInsertSql(fields, values);
            fields.append(',').append(COL_BARK_THICKNESS);
            values.append(',').append(getBarkThickness());
    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(this.barkThickness);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof AbstractTree))
			return false;
		AbstractTree other = (AbstractTree) obj;
		if (this.barkThickness != other.barkThickness)
			return false;
		System.out.println("ABSTRACTTREE MATCH");
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + ", barkThickness=" + this.barkThickness; //$NON-NLS-1$
	}
	
	public static void setColumnDefinitions() {
		columnDefinitions.put(COL_BARK_THICKNESS, COL_TYPE_VARCHAR_255);
	}
	
	public static void registerTree(Tree tree) {
		if (!allTrees.contains(tree))
			allTrees.add(tree);
	}

	
	
}
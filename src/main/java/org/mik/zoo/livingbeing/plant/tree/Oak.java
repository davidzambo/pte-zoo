package org.mik.zoo.livingbeing.plant.tree;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.mik.zoo.livingbeing.plant.PlantType;

public class Oak extends AbstractTree {
	
    public static final String SCIENTIFIC_NAME = "Quercus"; //$NON-NLS-1$
    public static final String IMAGE_URI = "https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/03/03/16/oak-tree-1.jpg"; //$NON-NLS-1$
    private static final int MAX_AGE = 300;
    private static final int MAX_HEIGHT = 134;
    private static final float BARK_THICKNESS = (float) 2.3;


	public Oak() {
		super(null, SCIENTIFIC_NAME, "", IMAGE_URI, MAX_AGE, MAX_HEIGHT, PlantType.TREE, BARK_THICKNESS); //$NON-NLS-1$
	}

	public Oak(Integer id, String scientificName, String instanceName, String imageURI, int maxAge, int maxHeight,
			PlantType plantType, float barkThickness) {
		super(id, scientificName, instanceName, imageURI, maxAge, maxHeight, plantType, barkThickness);
	}

	public Oak(ResultSet rs) throws SQLException {
		super(rs);
	}
	
	public Oak(String instanceName) {
		super(null, SCIENTIFIC_NAME, instanceName, IMAGE_URI, MAX_AGE, 
				MAX_HEIGHT, PlantType.TREE, BARK_THICKNESS);
	}
	
	public static Oak createFromResultSet(ResultSet rs) throws SQLException {
		return new Oak(rs);
	}
	
	@Override
	public String toString() {
		return "Oak:" + super.toString(); //$NON-NLS-1$
	}

}

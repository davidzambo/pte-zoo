package org.mik.zoo.livingbeing.plant.flower;

import org.mik.zoo.livingbeing.Color;

public interface Flower {
	
	public final static String COL_NUMBER_OF_BUDS = "number_of_buds"; //$NON-NLS-1$
	
	public final static String COL_COLOR_OF_BUDS = "color_of_buds"; //$NON-NLS-1$
	
	int getNumberOfBuds();
	
	Color getColorOfBuds();

}

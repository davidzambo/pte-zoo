package org.mik.zoo.livingbeing.plant.tree;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.mik.zoo.livingbeing.plant.PlantType;

public class Pine extends AbstractTree {

    public static final String SCIENTIFIC_NAME = "Pinus"; //$NON-NLS-1$
    public static final String IMAGE_URI = "https://upload.wikimedia.org/wikipedia/commons/d/d8/Pinus_ponderosa_8144t.jpg"; //$NON-NLS-1$
    private static final int MAX_AGE = 3000;
    private static final int MAX_HEIGHT = 65;
    private static final float BARK_THICKNESS = (float) 1.5;
	
	public Pine() {
		super(null, SCIENTIFIC_NAME, "", IMAGE_URI, MAX_AGE, MAX_HEIGHT, PlantType.TREE, BARK_THICKNESS); //$NON-NLS-1$
	}

	public Pine(Integer id, String scientificName, String instanceName, String imageURI, int maxAge, int maxHeight,
			PlantType plantType, float barkThickness) {
		super(id, scientificName, instanceName, imageURI, maxAge, maxHeight, plantType, barkThickness);
	}

	public Pine(ResultSet rs) throws SQLException {
		super(rs);
	}

	
	public Pine(String instanceName) {
		super(null, SCIENTIFIC_NAME, instanceName, IMAGE_URI, MAX_AGE, 
				MAX_HEIGHT, PlantType.TREE, BARK_THICKNESS);
	}
	
	public static Pine createFromResultSet(ResultSet rs) throws SQLException {
		return new Pine(rs);
	}
	
	@Override
	public String toString() {
		return "Pine:" + super.toString(); //$NON-NLS-1$
	}
}

package org.mik.zoo.livingbeing;

public enum Color {
	BLACK(0),
	WHITE(1),
	ROSE(2),
	VIOLET(3),
	MULTIPLE(4);
	
	private int value;
	
	public int getValue() {
		return this.value;
	}
	
	private Color(int value) {
		this.value = value;
	}
	
	public static Color fromInt(int val) {
		switch(val) {
		case 0: return BLACK;
		case 1: return WHITE;
		case 2: return ROSE;
		case 3: return VIOLET;
		default:
			return MULTIPLE;
		}
	}
}
